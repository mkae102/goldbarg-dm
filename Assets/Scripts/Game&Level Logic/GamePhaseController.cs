﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePhaseController : MonoBehaviour
{
    private int gamePhase = Constants.GAME_PHASE_ARSCAN;

    public int GetGamePhase()
    {
        return gamePhase;
    }

    public void SetGamePhase(int newPhase)
    {
        gamePhase = newPhase;
    }
}
