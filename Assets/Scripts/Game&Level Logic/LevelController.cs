﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    private LevelModel levelModel;
    private ButtonController buttonController;

    private void Start()
    {
        levelModel = gameObject.transform.parent.GetChild(0).gameObject.GetComponent<LevelModel>();
        levelModel.GamePhase = Constants.GAME_PHASE_BUILD;
        buttonController = GameObject.Find("IngameCanvas").GetComponent<ButtonController>();
        //Implement SetLevelText method in ButtonController
        buttonController.SetLevelDescription(GetLevelText());
    }

    public void WinLevel()
    {
        levelModel.WinLevel();

        //trigger WIN-animation
        buttonController.GetWinPanel().SetActive(true);
    }

    #region Physics Simulation

    public void StartSimulation()
    {

        Rigidbody[] rbs = GameObject.FindObjectsOfType<Rigidbody>();
        SaveInteractablesPositions(rbs);

        //deactivate is kinematic
        for (int i = 0; i < rbs.Length; i++)
        {
            if (rbs[i].GetComponent<Rigidbody>())
            {
                SetWindparticles(rbs[i], true);
                rbs[i].GetComponent<Rigidbody>().isKinematic = false;
            }
        }

        levelModel.GamePhase = Constants.GAME_PHASE_SIMULATION;
    }

    public void StopSimulation()
    {
        Rigidbody[] rbs = GameObject.FindObjectsOfType<Rigidbody>();

        Dictionary<int, PosRot> interactablesTemp = GetOriginalPositions();

        for (int i = 0; i < rbs.Length; i++)
        {
            int currentID = rbs[i].gameObject.GetInstanceID();
            if (interactablesTemp.ContainsKey(currentID))
            {
                SetWindparticles(rbs[i], false);
                rbs[i].transform.position = interactablesTemp[currentID].Pos;
                rbs[i].transform.rotation = interactablesTemp[currentID].Rot;
                rbs[i].isKinematic = true;
            }
        }

        levelModel.GamePhase = Constants.GAME_PHASE_BUILD;
    }

    private void CopyTransformFromTo(Transform source, Transform target)
    {
        target.position = source.position;
        target.rotation = source.rotation;
    }


    //add in model to interactables dictionary
    private void SaveInteractablesPositions(Rigidbody[] rigidbodiesToSave)
    {

        Dictionary<int, PosRot> tempObjects = new Dictionary<int, PosRot>();
        for (int i = 0; i < rigidbodiesToSave.Length; i++)
        {
            tempObjects.Add(rigidbodiesToSave[i].gameObject.GetInstanceID(), new PosRot(rigidbodiesToSave[i].gameObject.transform.position, rigidbodiesToSave[i].gameObject.transform.rotation));
        }

        //send to model
        levelModel.SavePositions(tempObjects);
    }

    private Dictionary<int, PosRot> GetOriginalPositions()
    {
        return levelModel.GetPositions();
    }
    #endregion

    private void SetWindparticles(Rigidbody rigidbody, bool active)
    {
        WindController windController = rigidbody.gameObject.GetComponentInChildren<WindController>(true);
        if (!( windController is null ))
        {
            windController.SetWind(active);
        }
    }

    public int[] GetRequiredGoalItemsIDs()
    {
        return levelModel.RequiredIDs;
    }

    public int GetGamePhase()
    {
        return levelModel.GamePhase;
    }

    public string GetLevelText()
    {
        return levelModel.GetLevelText();
    }

    public GameObject FindParentWithTag(GameObject childObject, string tag)
    {
        if (childObject == null)
        {
            return null;
        }
        if (childObject.tag.Equals(tag))
        {
            return childObject;
        }
        Transform t = childObject.transform;
        while (t.parent != null)
        {
            if (t.parent.tag == tag)
            {
                return t.parent.gameObject;
            }
            t = t.parent.transform;
        }
        return null; // Could not find a parent with given tag.
    }
}

public struct PosRot
{
    public PosRot(Vector3 pos, Quaternion rot)
    {
        Pos = pos;
        Rot = rot;
    }
    public Vector3 Pos
    {
        get;
    }
    public Quaternion Rot
    {
        get;
    }
}