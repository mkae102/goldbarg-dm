﻿using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameModel : MonoBehaviour
{
    public static GameModel model;

    public int maxLevel;
    private int currentLevel;

    public Dictionary<int, bool> collectables;
    public Dictionary<int, bool> levelsCompleted;

    public int CurrentLevel { get => currentLevel; set => currentLevel = value; }

    void Awake()
    {
        if(model == null)
        {
            DontDestroyOnLoad(gameObject);
            model = this;
        }
        else if(model != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        this.collectables = new Dictionary<int, bool>();
        this.levelsCompleted = new Dictionary<int, bool>();

        //default initializations
        this.collectables.Add(Constants.INTERACTIBLE_WEIGHT, true);
        this.collectables.Add(Constants.INTERACTIBLE_WIND, true);
        this.collectables.Add(Constants.INTERACTIBLE_MAGNET, true);
        this.collectables.Add(Constants.INTERACTIBLE_BALLOON, false);
        this.maxLevel = 6;
        this.levelsCompleted.Add(0, false);
        this.levelsCompleted.Add(1, false);
        this.levelsCompleted.Add(2, false);
        this.levelsCompleted.Add(3, false);
        this.levelsCompleted.Add(4, false);
        this.levelsCompleted.Add(5, false);

        Load();
    }

    internal void CompleteLevel(int levelID)
    {
        if(this.levelsCompleted[levelID] == false)
        {
            this.levelsCompleted[levelID] = true;
            IncrementLevel();
        }
    }

    #region Data Handling Methods

    private void IncrementLevel()
    {
        this.maxLevel++;
        Save();
    }

    public void Unlock(int collectable)
    {
        if (this.collectables.ContainsKey(collectable))
        {
            this.collectables[collectable] = true;
            Save();
        }
        else
        {
                Debug.Log("<color=red>Key not found: " + collectable + "</color>");
        }
    }

#endregion

#region SaveLoad Management
public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Create);
        PlayerData data = new PlayerData();

        //get data from scene
        data.maxLevel = maxLevel;
        data.collectables = collectables;
        data.levelsCompleted = levelsCompleted;

        //wrap the package and close it
        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData) bf.Deserialize(file);
            file.Close();
            //get the data from the file and put into the scene

            
            this.maxLevel = data.maxLevel;

            if (data.collectables != null)
            {
                this.collectables = data.collectables;
            }
            if (data.levelsCompleted != null)
            {
                this.levelsCompleted = data.levelsCompleted;
            }

        }
    }

    //autoload on start
    private void OnEnable()
    {
        Load();
    }
    //autosave on end
    private void OnDisable()
    {
        Save();
    }
    #endregion

    /*
    private void OnGUI()
    {
        //GUI.Label(new Rect(15, 30, 100, 30), this.levelsCompleted[ );
    }
    */
    
}

//the data to be stored
[Serializable]
class PlayerData
{
    public Dictionary<int, bool> collectables = new Dictionary<int, bool>();
    public int maxLevel;
    public Dictionary<int, bool> levelsCompleted;

    public string PrettyString()
    {
        string res = "";
        foreach (var level in levelsCompleted)
        {
            res = res + " Level: "+ level.Key + ", Completed: " +level.Value + "  -------  ";
        }
        return res;
    }
}

