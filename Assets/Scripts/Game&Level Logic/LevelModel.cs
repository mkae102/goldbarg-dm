﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelModel : MonoBehaviour
{
    public GameObject[] requiredItems;
    private int[] requiredIDs;
    public int levelID;
    public string levelText;

    private bool levelCompleted;

    private GameModel gameModel;
    private int gamePhase;

    Dictionary<int, PosRot> tempObjects;

    public int GamePhase { get => gamePhase; set => gamePhase = value; }
    public int[] RequiredIDs { get => requiredIDs; set => requiredIDs = value; }


    // Start is called before the first frame update
    void Start()
    {
        //get gameModel
        gameModel = Transform.FindObjectOfType<GameModel>().GetComponent<GameModel>();

        //register current level id in gameModel
        gameModel.CurrentLevel = levelID;

        levelCompleted = false;

        requiredIDs = new int[requiredItems.Length];
        for (int i = 0; i < requiredItems.Length; i++)
        {
            requiredIDs[i] = requiredItems[i].GetInstanceID();
        }

    }

    public void SavePositions(Dictionary<int, PosRot> interactables)
    {
        this.tempObjects = interactables;
    }

    public Dictionary<int, PosRot> GetPositions()
    {
        return this.tempObjects;
    }

    public void WinLevel()
    {
        if (!levelCompleted)
        {
            gameModel.CompleteLevel(this.levelID);
        }
    }

    internal string GetLevelText()
    {
        return this.levelText;
    }
}
