﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalController : MonoBehaviour
{
    // Start is called before the first frame update
    private List<Rigidbody> currentGoalItems;
    private GameObject goalItemCounterTextObject;
    private int[] requiredIDs;
    private LevelController levelController;

    private int origReqIDs;

    private Dictionary<int, bool> currentItemIDs;

    void Start()
    {
        levelController = gameObject.transform.root.GetChild(1).gameObject.GetComponent<LevelController>();
        currentGoalItems = new List<Rigidbody>();
        goalItemCounterTextObject = gameObject.transform.parent.GetChild(0).gameObject;
        requiredIDs = levelController.GetRequiredGoalItemsIDs();

        currentItemIDs = new Dictionary<int, bool>();
        origReqIDs = requiredIDs.Length;
        for (int i = 0; i < requiredIDs.Length; i++)
        {
            currentItemIDs.Add(requiredIDs[i], false);
        }
    }

    private void Update()
    {
        if (levelController.GetGamePhase() == Constants.GAME_PHASE_BUILD)
        {
            if (computeCurrentGoalItemsAmt() != 0)
            {
                for (int i = 0; i < requiredIDs.Length; i++)
                {
                    currentItemIDs[requiredIDs[i]] = false;
                }
                UpdateGoalCounterText();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject potentialGoalitem = other.gameObject;
        if (potentialGoalitem)
        {
            if (potentialGoalitem.GetComponent<Tags>())
            {
                if (potentialGoalitem.GetComponent<Tags>().HasTag(Constants.TAG_GOALITEM))
                {
                    for (int i = 0; i < requiredIDs.Length; i++)
                    {
                        if (potentialGoalitem.GetInstanceID() == requiredIDs[i])
                        {
                            if (!currentItemIDs[requiredIDs[i]])
                            {
                                currentItemIDs[requiredIDs[i]] = true;

                                //check for win
                                if (requiredIDs.Length <= computeCurrentGoalItemsAmt())
                                {
                                    levelController.WinLevel();
                                }
                            }
                        }
                    }
                    UpdateGoalCounterText();


                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GameObject potentialGoalitem = other.gameObject;
        if (potentialGoalitem)
        {
            if (potentialGoalitem.GetComponent<Tags>())
            {
                if (potentialGoalitem.GetComponent<Tags>().HasTag(Constants.TAG_GOALITEM))
                {
                    for (int i = 0; i < requiredIDs.Length; i++)
                    {
                        if (potentialGoalitem.GetInstanceID() == requiredIDs[i])
                        {
                            if (currentItemIDs[requiredIDs[i]])
                            {
                                currentItemIDs[requiredIDs[i]] = false;
                            }
                        }
                    }
                    UpdateGoalCounterText();
                }
            }
        }
    }

    private void UpdateGoalCounterText()
    {
        goalItemCounterTextObject.GetComponent<TextMesh>().text = "GoalItems in Goal: " + computeCurrentGoalItemsAmt();
    }

    private int computeCurrentGoalItemsAmt()
    {
        int res = 0;
        if (currentItemIDs != null)
        {
            foreach (var currentItemID in currentItemIDs)
            {
                if (currentItemID.Value)
                {
                    res++;
                }
            }
        }
        return res;
    }

}
