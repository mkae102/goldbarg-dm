﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tags : MonoBehaviour
{
    public bool magnetic = false;
    public bool attractorRepeller = false;
    public bool windable = false;
    public bool goalItem = false;

    IDictionary<int, bool> tagDictionary;

    public void Awake()
    {
        tagDictionary = new Dictionary<int, bool>()
                                            {
                                                {Constants.TAG_MAGNETIC, magnetic},
                                                {Constants.TAG_ATTRACTORREPELLER, attractorRepeller},
                                                {Constants.TAG_WINDABLE, windable},
                                                {Constants.TAG_GOALITEM, goalItem}
                                            };
    }

    public void SetTag(int tagId, bool value)
    {
        tagDictionary[tagId] = value;
    }

    // Tags tags = (Tags) GameObject.GetComponent(typeof(Tags)); 
    // --> tags.HasTag(Constants.TAG_...);
    public bool HasTag(int tagID)
    {
        bool result = false;
        if (tagDictionary.TryGetValue(tagID, out result))
        {
            return result;
        }
        else
        {
            return result;
        }
    }
}
