﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollPanelController : MonoBehaviour
{
    public GameObject scrollPanel;
    public GameObject button;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnClick()
    {
        SetScrollPanel(true);
    }

    public void ToggleScrollPanel()
    {
        scrollPanel.SetActive(!scrollPanel.activeSelf);
        //button.SetActive(!button.activeSelf);
    }

    public void SetScrollPanel(bool active)
    {
        scrollPanel.SetActive(active);
        //button.SetActive(!active);
    }

    public void SetButton(bool active)
    {
        button.SetActive(active);
    }
}
