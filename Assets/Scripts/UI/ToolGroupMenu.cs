﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolGroupMenu : MonoBehaviour
{
    public GameObject createModeGroup;
    public GameObject placeModeGroup;
    public Camera myCamera;

    private GameObject currentObject;
    public GameObject[] spawnObjects;

    //spawns an object in front of the camera
    public void SpawnObject(int id)
    {
        Debug.Log(currentObject);
        if(currentObject == null)
        {
            currentObject = Instantiate(spawnObjects[id], myCamera.transform);
        }
        EnterPlaceMode();
    }

    //places current object at it's position in the world
    public void PlaceObject()
    {
        Rigidbody rb = currentObject.GetComponent<Rigidbody>();
        //Detach object
        currentObject.transform.parent = null;
        //Enable Gravity
        rb.useGravity = true;
        //Get Current Velocity
        VelocityTracker velocityTracker = currentObject.GetComponent<VelocityTracker>();
        //transfer velocity to placed item
        rb.velocity = velocityTracker.GetVelocity();
        //Destroy VelocityTracker for better performance
        Destroy(velocityTracker);
        //Destroy Swipe Rotate Control
        Destroy(currentObject.GetComponent<SwipeRotate>());
        //make space for another currentObject
        currentObject = null;
        //Swap UI to Create Mode
        EnterCreateMode();
    }

    public void EnterPlaceMode() 
    {
        createModeGroup.SetActive(false);
        placeModeGroup.SetActive(true);
    }
    public void EnterCreateMode() 
    {
        placeModeGroup.SetActive(false);
        createModeGroup.SetActive(true);
    }

}
