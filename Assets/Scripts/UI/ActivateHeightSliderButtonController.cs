﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateHeightSliderButtonController : MonoBehaviour
{
    public GameObject button;
    public GameObject childObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        childObject.SetActive(!childObject.activeSelf);
    }
}
