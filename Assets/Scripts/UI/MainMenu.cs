﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public void NewGame()
    {
        //SceneManager.LoadScene("Content");
    }

    public void OpenMainArScene()
    {

        //SceneManager.LoadScene("ARScene");

    }

    public void LoadHowTo()
    {
        //SceneManager.LoadScene("Onboarding");
    }

    public void OpenPanel(GameObject Panel)
    {
        if (Panel != null)
        {
            Panel.SetActive(true);
        }
    }

    public void ClosePanel(GameObject Panel)
    {
        if (Panel != null)
        {
            Panel.SetActive(false);
        }
    }

    public void TogglePanel(GameObject Panel)
    {
        if (Panel != null)
        {
            bool isActive = Panel.activeSelf;
            Panel.SetActive(!isActive);
        }
    }

    // set Font to Bold when level is available
    public void ChangeToBoldFont(Button button)
    {

        button.GetComponentInChildren<Text>().fontStyle = FontStyle.Bold;
    }

    public void ChangeScene(string SceneName)
    {

        //SceneManager.LoadScene(SceneName);
    }
}

