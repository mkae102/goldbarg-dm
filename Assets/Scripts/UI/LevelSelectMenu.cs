﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectMenu : MonoBehaviour
{
    public void LoadLevel(int level_ID)
    {
        //at the moment just take the next scene
        string sceneName = "Level_" + level_ID.ToString();
        SceneManager.LoadScene(sceneName);

    }
}
