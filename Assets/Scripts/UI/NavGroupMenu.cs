﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class NavGroupMenu : MonoBehaviour
{

    public void Back()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void PlayPhysics()
    {
        
    }
}
