﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    public InteractionController interactionController;
    public ObjectManipulationController objectManipulationController;

    public GameObject applyButton;
    public GameObject selectedObjectInteractionPanel;
    public GameObject addPlaygroundButton;
    public GameObject simulationButtonPanel;
    public GameObject levelSelectionPanel;
    public GameObject toolBoxPanelController;
    public GameObject mainMenuButton;
    public GameObject levelPanel;
    public GameObject winPanel;

    public TextMeshProUGUI levelDescriptionText;

    private void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (interactionController.selectedObjectState == Constants.OBJECT_STATE_NONE)
        {
            applyButton.SetActive(false);
            selectedObjectInteractionPanel.SetActive(false);
        }
        else if (interactionController.selectedObjectState == Constants.OBJECT_STATE_PLACING)
        {
            applyButton.SetActive(interactionController.IsSelectedObjectActive());
        }
        selectedObjectInteractionPanel.SetActive(interactionController.GetGamePhase() == Constants.GAME_PHASE_BUILD && interactionController.selectedObjectState == Constants.OBJECT_STATE_PLACED);
        simulationButtonPanel.SetActive(( interactionController.GetGamePhase() == Constants.GAME_PHASE_BUILD || interactionController.GetGamePhase() == Constants.GAME_PHASE_SIMULATION ) && interactionController.selectedObjectState == Constants.OBJECT_STATE_NONE);
    }

    public void OnClickTrashButton()
    {
        objectManipulationController.RemoveObject(interactionController.GetSelectedGameObject());
    }

    public void OnClickPlaygroundButton()
    {
        objectManipulationController.CreatePlayground();
        addPlaygroundButton.SetActive(false);
        applyButton.SetActive(true);
    }

    public void OnClickApplyButton()
    {
        objectManipulationController.PlaceSelectedObject();
        toolBoxPanelController.SetActive(true);
    }

    public void OnClickStartSimulationButton()
    {
        mainMenuButton.SetActive(false);
        toolBoxPanelController.SetActive(false);
        interactionController.StartSimulation();
    }

    public void OnClickStopSimulationButton()
    {
        mainMenuButton.SetActive(true);
        toolBoxPanelController.SetActive(true);
        interactionController.StopSimulation();
    }

    public void OnClickCreateObjectButton(GameObject gameObject)
    {
        objectManipulationController.CreateObject(gameObject);
        toolBoxPanelController.SetActive(false);
    }

    public void OnClickLevelSelectionButton(GameObject gameObject)
    {
        winPanel.SetActive(false);
        interactionController.DeleteLevel();
        interactionController.playgroundPrefab = gameObject;
        levelSelectionPanel.SetActive(false);
        addPlaygroundButton.SetActive(true);
        toolBoxPanelController.SetActive(false);
    }

    public void OnClickRotateLeft()
    {
        objectManipulationController.RotateObject(interactionController.GetSelectedGameObject(), -15);
    }

    public void OnClickRotateRight()
    {
        objectManipulationController.RotateObject(interactionController.GetSelectedGameObject(), 15);
    }

    public void OnClickToggleObject(GameObject gameObject)
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public void SetLevelDescription(string description)
    {
        levelPanel.SetActive(true);
        levelDescriptionText.SetText(description);
    }

    public void OpenPanel(GameObject Panel)
    {
        if (Panel != null)
        {
            Panel.SetActive(true);
        }
    }

    public void ClosePanel(GameObject Panel)
    {
        if (Panel != null)
        {
            Panel.SetActive(false);
        }
    }

    public GameObject GetWinPanel()
    {
        return winPanel;
    }
}

