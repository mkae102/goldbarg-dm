﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

/// <summary>
/// Interaction Controller
/// </summary>
public class InteractionController : MonoBehaviour
{
    LevelController levelController;
    public GamePhaseController gamePhaseController;
    public GameObject playgroundPrefab;
    private GameObject selectedGameObject;
    public int selectedObjectState;
    DetectedPlaneController detectedPlaneController;
    AR_SessionController ar_SessionController;

    void Awake()
    {
        detectedPlaneController = GetComponent<DetectedPlaneController>();
        ar_SessionController = GetComponent<AR_SessionController>();

        DeleteLevel();
    }

    public void DeleteLevel()
    {
        GameObject[] rootObjects = GameObject.FindGameObjectsWithTag("RootObject");
        foreach (GameObject i in rootObjects)
        {
            Destroy(i);
        }
        selectedObjectState = Constants.OBJECT_STATE_NONE;
        SetGamePhase(Constants.GAME_PHASE_ARSCAN);
        SetTrackableFloor(true);
    }

    public void SetTrackableFloor(bool active)
    {
        if (Application.isEditor)
        {
            ar_SessionController.SetTestingGround(active);
        }
        else
        {
            detectedPlaneController.SetPlaneDetection(active);
        }

    }

    public void ToggleGameObject(GameObject gameObject)
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    public GameObject[] FindGameObjectsWithLayer(int layer)
    {
        GameObject[] goArray = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        var goList = new List<GameObject>();
        for (int i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == layer)
            {
                goList.Add(goArray[i]);
            }
        }
        if (goList.Count == 0)
        {
            return null;
        }
        return goList.ToArray();
    }

    public GameObject GetSelectedGameObject()
    {
        return selectedGameObject;
    }

    public void SetSelectedGameObject(GameObject gameObject)
    {
        //selectedGameObject = gameObject;
        selectedGameObject = FindParentWithTag(gameObject, "RootObject");
    }

    public void StartSimulation()
    {
        levelController.StartSimulation();
        gamePhaseController.SetGamePhase(Constants.GAME_PHASE_SIMULATION);
    }

    public void StopSimulation()
    {
        levelController.StopSimulation();
        gamePhaseController.SetGamePhase(Constants.GAME_PHASE_BUILD);
    }

    public void FindLevelController()
    {
        levelController = GameObject.FindObjectOfType<LevelController>();
    }

    public GameObject GetPlaygroundPrefab()
    {
        return playgroundPrefab;
    }

    public int GetGamePhase()
    {
        return gamePhaseController.GetGamePhase();
    }

    public void SetGamePhase(int newPhase)
    {
        gamePhaseController.SetGamePhase(newPhase);
    }

    public GameObject FindParentWithTag(GameObject childObject, string tag)
    {
        if (childObject == null)
        {
            return null;
        }
        if (childObject.tag.Equals(tag))
        {
            return childObject;
        }
        Transform t = childObject.transform;
        while (t.parent != null)
        {
            if (t.parent.tag == tag)
            {
                return t.parent.gameObject;
            }
            t = t.parent.transform;
        }
        return null; // Could not find a parent with given tag.
    }

    public bool IsSelectedObjectActive()
    {
        return selectedGameObject.activeSelf;
    }
}