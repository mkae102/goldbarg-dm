﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class UserInputController : MonoBehaviour
{
    InteractionController interactionController;
    ARRaycastManager m_RaycastManager;
    public ButtonController buttonController;
    ObjectManipulationController objectManipulationController;
    Camera mainCamera;

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    public LayerMask testingLayerMask;
    public LayerMask interactibleLayerMask;
    public LayerMask buildSurfaceLayerMask;
    public LayerMask uiLayerMask;

    // Start is called before the first frame update
    void Awake()
    {
        interactionController = GetComponent<InteractionController>();
        m_RaycastManager = GetComponent<ARRaycastManager>();
        objectManipulationController = GetComponent<ObjectManipulationController>();
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (interactionController.GetGamePhase() != Constants.GAME_PHASE_SIMULATION)
        {
            if (interactionController.selectedObjectState == Constants.OBJECT_STATE_PLACING)
            {
                MoveSelectedObjectToCameraCenter();
            }
        }
    }

    void LateUpdate()
    {
        if (interactionController.GetGamePhase() != Constants.GAME_PHASE_SIMULATION)
        {
            HandleUserInput();
        }
    }

    private void MoveSelectedObjectToCameraCenter()
    {
        Ray ray;
        ///Move selected object to centrepoint of camera that hits the scanned floor
        if (interactionController.GetGamePhase() == Constants.GAME_PHASE_SETUPPLAYGROUND)
        {
            if (Application.isEditor)
            {
                ray = Camera.main.ScreenPointToRay(new Vector3(Camera.main.pixelWidth * 0.5f, Camera.main.pixelHeight * 0.5f, 0f));
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 500f, testingLayerMask))
                {
                    objectManipulationController.MoveSelectedObject(hit.point, false);
                }
                else
                {
                    objectManipulationController.HideSelectedObject();
                }
            }
            else
            {
                ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                if (m_RaycastManager.Raycast(ray, s_Hits, TrackableType.PlaneWithinPolygon))
                {
                    Pose hitPose = s_Hits[0].pose;
                    objectManipulationController.MoveSelectedObject(hitPose.position, false);
                }
                else
                {
                    objectManipulationController.HideSelectedObject();
                }
            }
        }
        else if (interactionController.GetGamePhase() == Constants.GAME_PHASE_BUILD)
        {
            if (Application.isEditor)
            {
                ray = Camera.main.ScreenPointToRay(new Vector3(Camera.main.pixelWidth * 0.5f, Camera.main.pixelHeight * 0.5f, 0f));
            }
            else
            {

                ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            }

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 500f, buildSurfaceLayerMask))
            {
                objectManipulationController.MoveSelectedObject(hit.point, false);
            }
            else
            {
                objectManipulationController.HideSelectedObject();
            }
        }
    }

    private void HandleUserInput()
    {
        if (( interactionController.selectedObjectState == Constants.OBJECT_STATE_NONE || interactionController.selectedObjectState == Constants.OBJECT_STATE_PLACED )
            && interactionController.GetGamePhase() == Constants.GAME_PHASE_BUILD)
        {
            if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.currentSelectedGameObject != null)
            {
                return;
            }
            if (Application.isEditor)
            {
                HandleEditorInput();

            }
            else
            {
                HandleTouchInput();
            }
        }
    }

    private void HandleTouchInput()
    {
        /// One Finger Touch
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = mainCamera.ScreenPointToRay(touch.position);
                CheckRaycast(ray);
            }
            else if (touch.phase == TouchPhase.Moved && interactionController.GetSelectedGameObject() != null)
            {
                //if (m_RaycastManager.Raycast(touch.position, s_Hits, TrackableType.PlaneWithinPolygon))
                //{
                //    Pose hitPose = s_Hits[0].pose;
                //    MoveSelectedObject(hitPose.position, false);
                //}

                var ray = mainCamera.ScreenPointToRay(touch.position);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 500f, buildSurfaceLayerMask))
                {
                    objectManipulationController.MoveSelectedObject(hit.point, false);
                }
            }
        }
        ///Two Finger Touch
        else if (Input.touchCount == 2)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                ///Rotate
                if (interactionController.GetSelectedGameObject() != null)
                {
                    if (interactionController.GetSelectedGameObject().activeSelf == true)
                    {
                        Quaternion desiredRotation = interactionController.GetSelectedGameObject().transform.rotation;
                        DetectTouchMovement.Calculate();
                        if (Mathf.Abs(DetectTouchMovement.turnAngleDelta) > 0)
                        {
                            float rotationDegreeY = -DetectTouchMovement.turnAngleDelta;
                            objectManipulationController.RotateObject(interactionController.GetSelectedGameObject(), rotationDegreeY);
                        }
                    }
                }
                /////Scale
                //else
                //{
                //    float pinchAmount = 0;
                //    DetectTouchMovement.Calculate();
                //    if (Mathf.Abs(DetectTouchMovement.pinchDistanceDelta) > 0)
                //    {
                //        pinchAmount = DetectTouchMovement.pinchDistanceDelta;
                //    }
                //    pinchAmount = pinchAmount * 0.001f;
                //    Vector3 newScale = mainScale;
                //    newScale += pinchAmount * mainScale;
                //    interactionController.ScaleAllObjects(newScale);
                //}
            }
        }
    }

    private void HandleEditorInput()
    {
        var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            CheckRaycast(ray);
        }
        if (Input.GetMouseButton(0) && interactionController.GetSelectedGameObject() != null)
        {
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 500f, buildSurfaceLayerMask))
            {
                objectManipulationController.MoveSelectedObject(hit.point, false);
            }
        }
        if (Input.GetMouseButton(1) && interactionController.GetSelectedGameObject() != null)
        {
            CheckRaycast(ray);
        }
        ///Rotate with Mousewheel
        if (interactionController.GetSelectedGameObject() != null)
        {
            RaycastHit hit2;
            if (Physics.Raycast(ray, out hit2, 500f))
            {
                float pinchAmount = Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 20;
                if (pinchAmount != 0)
                {
                    //Quaternion oldRotation = interactionController.GetSelectedGameObject().transform.rotation;
                    //float newY = oldRotation.y * pinchAmount;
                    //interactionController.GetSelectedGameObject().transform.rotation = new Quaternion(oldRotation.x, newY, oldRotation.z, Space.World);
                    float rotationDegreeY = pinchAmount * 1000;
                    objectManipulationController.RotateObject(interactionController.GetSelectedGameObject(), rotationDegreeY);
                }
            }
        }
    }



    /// <summary>
    /// Check if the Ray hits an Interactible, then select it. Otherwise unselect the current selected object.
    /// </summary>
    /// <param name="ray"></param>
    private void CheckRaycast(Ray ray)
    {
        RaycastHit hit;
        //GameObject resultObject;
        ///Raycast to see if an Interactible has been hit
        if (Physics.Raycast(ray, out hit, 500f, interactibleLayerMask))
        {
            GameObject tmpObject = hit.collider.gameObject;
            objectManipulationController.SetSelected(tmpObject);
            interactionController.selectedObjectState = Constants.OBJECT_STATE_PLACED;
        }
        else if (!Physics.Raycast(ray, out hit, 500f, uiLayerMask))
        {
            if (interactionController.GetSelectedGameObject() != null)
            {
                objectManipulationController.Unselect();
            }
            //buttonController.toolBoxPanelController.SetScrollPanel(false);
        }
    }
}
