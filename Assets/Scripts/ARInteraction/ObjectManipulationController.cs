﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManipulationController : MonoBehaviour
{
    InteractionController interactionController;
    //DetectedPlaneController detectedPlaneController;
    //AR_SessionController ar_SessionController;

    private Vector3 mainScale = Vector3.zero;
    private Vector3 lastPlacementPosition;
    private float minLocalHeight = 0;

    public GameObject selectionVisualization;
    public float lerpSpeed = 3f;

    // Start is called before the first frame update
    void Awake()
    {
        interactionController = GetComponent<InteractionController>();
        //detectedPlaneController = GetComponent<DetectedPlaneController>();
        //ar_SessionController = GetComponent<AR_SessionController>();
        //detectedPlaneController.SetPlaneDetection(true);
    }

    ///Create GameObject, it should follow the Camera as long as it points at a Plane
    public void CreateObject(GameObject gameObject)
    {
        SetAllCollidersStatus(interactionController.GetSelectedGameObject(), true);
        if (interactionController.selectedObjectState == Constants.OBJECT_STATE_PLACING)
        {
            RemoveObject(interactionController.GetSelectedGameObject());
        }
        GameObject createdObject = Instantiate(gameObject, new Vector3(0, 0, 0), Quaternion.identity);
        SetSelected(createdObject);
        if (mainScale.Equals(Vector3.zero))
        {
            mainScale = interactionController.GetSelectedGameObject().transform.localScale;
        }
        //selectedGameObject.transform.parent = mainCamera.transform;
        interactionController.selectedObjectState = Constants.OBJECT_STATE_PLACING;
    }

    public void CreatePlayground()
    {
        interactionController.SetGamePhase(Constants.GAME_PHASE_SETUPPLAYGROUND);
        CreateObject(interactionController.GetPlaygroundPrefab());
        interactionController.FindLevelController();
    }

    //Remove GameObject
    public void RemoveObject(GameObject gameObject)
    {
        Destroy(gameObject);
        selectionVisualization.SetActive(false);
        interactionController.selectedObjectState = Constants.OBJECT_STATE_NONE;
    }

    public void MoveSelectedObject(Vector3 newPosition, bool autoRotate)
    {
        MoveObject(interactionController.GetSelectedGameObject(), newPosition, autoRotate);
    }

    private void MoveObject(GameObject gameObject, Vector3 newPosition, bool autoRotate)
    {
        lastPlacementPosition = newPosition;
        gameObject.SetActive(true);
        selectionVisualization.SetActive(true);
        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, newPosition, Time.deltaTime * lerpSpeed);
        selectionVisualization.transform.position = Vector3.Lerp(gameObject.transform.position, newPosition, Time.deltaTime * lerpSpeed);
        ///Rotate towards camera
        if (autoRotate)
        {
            Vector3 cameraFlatPosition = new Vector3(Camera.main.transform.position.x, newPosition.y, Camera.main.transform.position.z);
            gameObject.transform.LookAt(cameraFlatPosition);
        }
    }

    public void HideSelectedObject()
    {
        HideObject(interactionController.GetSelectedGameObject());
    }

    private void HideObject(GameObject gameObject)
    {
        if (gameObject.activeSelf == true)
        {
            gameObject.SetActive(false);
            selectionVisualization.SetActive(false);
            //selectedGameObject.transform.parent = Camera.main.transform;
            gameObject.transform.localPosition = Vector3.zero;
        }
    }

    //Place Object where Camera is pointing
    public void PlaceSelectedObject()
    {
        PlaceObject(interactionController.GetSelectedGameObject());
    }

    private void PlaceObject(GameObject gameObject)
    {
        ParticleSystem[] particleSystems = gameObject.GetComponentsInChildren<ParticleSystem>(true);
        if (particleSystems != null)
        {
            if (particleSystems.Length > 0)
            {
                for (int i = 0; i < particleSystems.Length; i++)
                {
                    if (particleSystems[i].name.Equals("PlacementParticles"))
                    {
                        particleSystems[i].Play();
                    }
                }
            }
        }
        if (interactionController.GetGamePhase() == Constants.GAME_PHASE_SETUPPLAYGROUND)
        {
            //minLocalHeight = gameObject.transform.localPosition.y;
            minLocalHeight = gameObject.transform.position.y;
        }
        gameObject.transform.position = lastPlacementPosition;
        selectionVisualization.transform.position = gameObject.transform.position;
        Unselect();
        ///Disable Trackable Floor when Playground is placed
        if (interactionController.GetGamePhase() == Constants.GAME_PHASE_SETUPPLAYGROUND)
        {
            interactionController.SetGamePhase(Constants.GAME_PHASE_BUILD);
            interactionController.SetTrackableFloor(false);
        }
    }

    public void SetSelected(GameObject gameObject)
    {
        Unselect();
        if (interactionController.GetGamePhase() == Constants.GAME_PHASE_BUILD && interactionController.GetSelectedGameObject() != null)
        {
            SetObjectTransparent(interactionController.GetSelectedGameObject(), false);
        }
        interactionController.SetSelectedGameObject(gameObject);
        SetAllCollidersStatus(interactionController.GetSelectedGameObject(), false);
        selectionVisualization.SetActive(true);
        Vector3 selectedObjectPosition = interactionController.GetSelectedGameObject().transform.position;
        selectionVisualization.transform.position = new Vector3(selectedObjectPosition.x, 0, selectedObjectPosition.z);
        //selectedGameObject.layer = 9;
        if (interactionController.GetGamePhase() == Constants.GAME_PHASE_BUILD)
        {
            SetObjectTransparent(interactionController.GetSelectedGameObject(), true);
        }
    }

    public void Unselect()
    {
        //GameObject selectedGameObject = interactionController.GetSelectedGameObject();
        if (interactionController.GetGamePhase() == Constants.GAME_PHASE_BUILD && interactionController.GetSelectedGameObject() != null)
        {
            SetObjectTransparent(interactionController.GetSelectedGameObject(), false);
        }
        SetAllCollidersStatus(interactionController.GetSelectedGameObject(), true);
        //selectedGameObject.layer = 10;
        interactionController.SetSelectedGameObject(null);
        interactionController.selectedObjectState = Constants.OBJECT_STATE_NONE;
        selectionVisualization.SetActive(false);
    }

    /// <summary>
    /// Scales all interactibles at once
    /// </summary>
    private void ScaleAllObjects(Vector3 newScale)
    {
        ///Below the Min and Max scale can be adjusted
        if (newScale.x > 0.2 && newScale.x < 10)
        {
            //change scale of every main and secondary GameObject
            GameObject[] gameObjects = interactionController.FindGameObjectsWithLayer(10);
            if (gameObjects == null)
            {
                return;
            }
            foreach (GameObject tmpObject in gameObjects)
            {
                //tmpObject.transform.localScale = newScale;
            }
            mainScale = newScale;
        }
    }

    public void RotateObject(GameObject gameObject, float rotationDegreeY)
    {
        Quaternion desiredRotation = gameObject.transform.rotation;
        Vector3 rotationDeg = Vector3.zero;
        rotationDeg.y = rotationDegreeY;
        desiredRotation *= Quaternion.Euler(rotationDeg);
        desiredRotation.x = 0;
        desiredRotation.z = 0;
        gameObject.transform.rotation = desiredRotation;
    }

    public void ChangeLocalPosition(GameObject gameObject, Vector3 newPosition)
    {
        gameObject.transform.position = newPosition;
        //if (newPosition.y >= minLocalHeight)
        //{
        //    //gameObject.transform.localPosition = newPosition;
        //    gameObject.transform.position = newPosition;
        //}
    }

    private void SetObjectTransparent(GameObject gameObject, bool isTransparent)
    {
        float transparency = isTransparent ? 60f : 255f;
        Color color = gameObject.GetComponentInChildren<Renderer>().material.color;
        gameObject.GetComponentInChildren<Renderer>().material.color = new Color(color.r, color.g, color.b, colorNumberConversion(transparency));
    }

    private float colorNumberConversion(float num)
    {
        return ( num / 255.0f );
    }

    public void SetAllCollidersStatus(GameObject gameObject, bool active)
    {
        if (gameObject != null)
        {
            foreach (Collider c in gameObject.GetComponentsInChildren<Collider>())
            {
                c.enabled = active;
            }
        }
    }

    public float GetMinLocalHeight()
    {
        return minLocalHeight;
    }
}
