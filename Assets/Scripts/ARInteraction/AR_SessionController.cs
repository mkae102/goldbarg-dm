﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Session Controller
/// </summary>
public class AR_SessionController : MonoBehaviour
{
    public GameObject[] testingGround;
    private Camera mainCamera;

    void Awake()
    {
        mainCamera = Camera.main;
        ///if testing in editor move camera higher and activate testing ground
        if (Application.isEditor)
        {
            Vector3 newCameraPosition = new Vector3(0, 1.5f, 0);
            Quaternion newCameraRotation = Quaternion.Euler(40, 0, 0);
            mainCamera.transform.position = newCameraPosition;
            mainCamera.transform.rotation = newCameraRotation;
        }
        SetTestingGround(Application.isEditor);
    }

    public void SetTestingGround(bool isActive)
    {
        for (int i = 0; i < testingGround.Length; i++)
        {
            testingGround[i].SetActive(isActive);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Reset()
    {
        SceneManager.LoadScene("ARScene");
    }
}
