﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectHeightController : MonoBehaviour
{
    ObjectManipulationController objectManipulationController;
    InteractionController interactionController;

    // Start is called before the first frame update
    void Awake()
    {
        interactionController = GetComponent<InteractionController>();
        objectManipulationController = GetComponent<ObjectManipulationController>();
    }

    public Vector3 GetSelectedObjectLocalPosition()
    {
        GameObject selectedObject = interactionController.GetSelectedGameObject();
        //return selectedObject.transform.localPosition;
        return selectedObject.transform.position;
    }

    public void AdjustSelectedObjectHeight(float heightAdjustment)
    {
        Vector3 currentPosition = GetSelectedObjectLocalPosition();
        float calculateHeight = currentPosition.y + heightAdjustment;
        float newHeight = calculateHeight > objectManipulationController.GetMinLocalHeight() ? calculateHeight : objectManipulationController.GetMinLocalHeight();
        Vector3 newPosition = new Vector3(currentPosition.x, newHeight, currentPosition.z);
        objectManipulationController.ChangeLocalPosition(interactionController.GetSelectedGameObject(), newPosition);
    }

    public void AddHeight()
    {
        AdjustSelectedObjectHeight(CheckDistance(interactionController.GetSelectedGameObject()));
    }

    public void ReduceHeight()
    {
        AdjustSelectedObjectHeight(-CheckDistance(interactionController.GetSelectedGameObject()));
    }

    public float CheckDistance(GameObject gameObject)
    {
        //float minimumDistance = 0;
        //float maximumDistance = 1;
        //var distance = ( gameObject.transform.position - Camera.main.transform.position ).magnitude;
        //var norm = ( distance - minimumDistance ) / ( maximumDistance - minimumDistance );
        //norm = Mathf.Clamp01(norm);
        //return norm;

        float increment = 0.1f;
        float distance = Vector3.Distance(gameObject.transform.position, Camera.main.transform.position);
        if (distance < 1)
        {
            increment = 0.05f;
        }
        else if (distance > 1 && distance < 2)
        {
            increment = 0.1f;
        }
        else if (distance > 2)
        {
            increment = 0.2f;
        }
        return increment;
    }
}
