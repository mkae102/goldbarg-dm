﻿public class Constants
{
    public const int OBJECT_STATE_NONE = 0;
    public const int OBJECT_STATE_PLACING = 1;
    public const int OBJECT_STATE_PLACED = 2;

    public const int GAME_PHASE_ARSCAN = 0;
    public const int GAME_PHASE_SETUPPLAYGROUND = 1;
    public const int GAME_PHASE_BUILD = 2;
    public const int GAME_PHASE_SIMULATION = 3;

    public const int TAG_MAGNETIC = 0;
    public const int TAG_ATTRACTORREPELLER = 1;
    public const int TAG_WINDABLE = 2;
    public const int TAG_GOALITEM = 3;
    public const int TAG_INTERACTIBLE = 4;

    public const int INTERACTIBLE_WEIGHT = 0;
    public const int INTERACTIBLE_WIND = 1;
    public const int INTERACTIBLE_MAGNET = 2;
    public const int INTERACTIBLE_BALLOON = 3;
}
