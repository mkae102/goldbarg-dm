﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class spawnAndPlaceToggle : MonoBehaviour
{
    Toggle m_Toggle;
    public Text m_Text;
    public float offset;
    public GameObject spawnObject;
    private GameObject currentObject;

    void Start()
    {
        //Fetch the Toggle GameObject
        m_Toggle = GetComponent<Toggle>();
        //Add listener for when the state of the Toggle changes, to take action
        m_Toggle.onValueChanged.AddListener(delegate {
            ToggleValueChanged(m_Toggle);
        });

        //Initialise the Text to say the first state of the Toggle
        m_Text.text = "First Value : " + m_Toggle.isOn;
    }

    //Output the new state of the Toggle into Text
    void ToggleValueChanged(Toggle change)
    {
        //change from true to false
        if (!m_Toggle.isOn)
        {
            SpawnObject();
            Debug.Log("changed from true to false");
        } else
        {
            PlaceObject();
        }
        m_Text.text = "New Value : " + m_Toggle.isOn;
    }

    public void SpawnObject()
    {
        if (currentObject == null)
        {
            Camera mainCamera = Camera.main;
            Vector3 cameraPos = mainCamera.transform.position;
            Vector3 cameraDirection = mainCamera.transform.forward;
            Quaternion cameraRotation = mainCamera.transform.rotation;
            Vector3 spawnPos = cameraPos + cameraDirection * offset;
            currentObject = Instantiate(spawnObject, spawnPos, cameraRotation);
            currentObject.transform.parent = mainCamera.transform;
        }
    }

    //places current object at it's position in the world
    public void PlaceObject()
    {
        currentObject.transform.parent = null;
        currentObject = null;
    }
}
