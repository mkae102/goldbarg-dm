﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityTracker : MonoBehaviour
{
    private Vector3 PrevPos;
    private Vector3 NewPos;
    private Vector3 ObjVelocity;

    // Start is called before the first frame update
    void Start()
    {
        PrevPos = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        NewPos = transform.position;  // each frame track the new position
        ObjVelocity = (NewPos - PrevPos) / Time.fixedDeltaTime;  // velocity = dist/time
        PrevPos = NewPos;  // update position for next frame calculation
    }

    public Vector3 GetVelocity()
    {
        return ObjVelocity;
    }
}
