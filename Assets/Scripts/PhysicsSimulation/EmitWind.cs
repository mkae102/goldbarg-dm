﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitWind : MonoBehaviour
{
    public float forceFactor = 1f;
    List<Rigidbody> otherRGBodies = new List<Rigidbody>();

    private void Start()
    {
    }

    private void FixedUpdate()
    {
            foreach (var rigidbody in otherRGBodies)
            {
                //distance vector
                Vector3 selfpos = gameObject.transform.position;
                Vector3 distanceVec = selfpos - rigidbody.transform.position;

                //distance
                float sqrDistance = distanceVec.sqrMagnitude;

                rigidbody.AddForce((-1) * distanceVec * forceFactor * (1 / sqrDistance * 10));
            }
        
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Windable")) { 
            otherRGBodies.Add(other.attachedRigidbody);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Windable"))
        {
            otherRGBodies.Remove(other.attachedRigidbody);
        }
    }

}
