﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractorRepellerController : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject[] magneticObjects;

    public float magneticForce;
    public float forceRadius;

    void Start()
    {
        //Search for objects to move
        magneticObjects = GameObject.FindGameObjectsWithTag("Magnetic");
        
        magneticForce = gameObject.GetComponent<Rigidbody>().mass * magneticForce;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        for (int i = 0; i < magneticObjects.Length; i++)
        {
            //current magnetic object
            GameObject currentObject = magneticObjects[i];

            //Vector pointing from magnetic Object to Source
            Vector3 towardsSource = gameObject.transform.position - currentObject.transform.position;

            //get distance
            float distance = towardsSource.sqrMagnitude;
            

            if(distance < forceRadius*forceRadius)
            {
                //normalize
                Vector3 direction = towardsSource.normalized;

                //apply magnetic force to magnetic objects

                Vector3 pull = direction * (1 / distance) * magneticForce;

                //if(pull.sqrMagnitude <)
            
                currentObject.GetComponent<Rigidbody>().AddForce(pull); ;
            }

        }
    }
}
