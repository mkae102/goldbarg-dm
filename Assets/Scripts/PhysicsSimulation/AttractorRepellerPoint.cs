﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractorRepellerPoint : MonoBehaviour
{
    public float forceFactor = 1f;
    List<Rigidbody> otherRGBodies = new List<Rigidbody>();


    private void FixedUpdate()
    {
        
        foreach (var rigidbody in otherRGBodies)
        {
            //distance vector
            Vector3 distanceVec = gameObject.transform.position - rigidbody.transform.position;

            //distance
            float sqrDistance = distanceVec.sqrMagnitude;

            rigidbody.AddForce(distanceVec * forceFactor * (1/ sqrDistance*10));
        }
        
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Tags>())
        {
            if (other.gameObject.GetComponent<Tags>().HasTag(Constants.TAG_MAGNETIC))
            {
                otherRGBodies.Add(other.attachedRigidbody);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Tags>())
        {
            if (other.gameObject.GetComponent<Tags>().HasTag(Constants.TAG_MAGNETIC))
            {
                otherRGBodies.Remove(other.attachedRigidbody);
            }
        }
    }
}
