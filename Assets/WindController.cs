﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindController : MonoBehaviour
{
    public float forceFactor = 1f;
    List<Rigidbody> otherRGBodies = new List<Rigidbody>();
    private ParticleSystem PSystem;
    public List<ParticleCollisionEvent> collisionEvents;

    // Start is called before the first frame update
    void Start()
    {
        PSystem = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }


    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = PSystem.GetCollisionEvents(other, collisionEvents);

        Rigidbody rb = other.GetComponentInParent<Rigidbody>();
        int i = 0;

        while (i < numCollisionEvents)
        {
            if (rb)
            {
                if (rb.isKinematic == false)
                {
                    Tags tags = rb.GetComponentInParent<Tags>();
                    if (tags != null)
                    {
                        if (tags.HasTag(Constants.TAG_WINDABLE))
                        {
                            //distance vector
                            Vector3 selfpos = gameObject.transform.position;
                            Vector3 distanceVec = selfpos - rb.transform.position;

                            //distance
                            float sqrDistance = distanceVec.sqrMagnitude;

                            rb.AddForce(( -1 ) * distanceVec * forceFactor * ( 1 / sqrDistance * 10 ));
                        }
                    }
                }
            }
            i++;
        }
    }

    public void StartWind()
    {
        ParticleSystem particleSystem = gameObject.GetComponentInChildren<ParticleSystem>(true);
        if (particleSystem != null)
        {
            particleSystem.Play();
        }
    }

    public void StopWind()
    {
        ParticleSystem particleSystem = gameObject.GetComponentInChildren<ParticleSystem>(true);
        if (particleSystem != null)
        {
            particleSystem.Stop();
        }
    }

    public void SetWind(bool active)
    {
        ParticleSystem particleSystem = gameObject.GetComponentInChildren<ParticleSystem>(true);
        if (particleSystem != null)
        {
            if (active)
            {

                particleSystem.Play();
            }
            else
            {
                particleSystem.Stop();
                particleSystem.Clear();
            }
        }
    }

    public GameObject FindParentWithTag(GameObject childObject, string tag)
    {
        if (childObject == null)
        {
            return null;
        }
        if (childObject.tag.Equals(tag))
        {
            return childObject;
        }
        Transform t = childObject.transform;
        while (t.parent != null)
        {
            if (t.parent.tag == tag)
            {
                return t.parent.gameObject;
            }
            t = t.parent.transform;
        }
        return null; // Could not find a parent with given tag.
    }

}
