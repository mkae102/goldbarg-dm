﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncrementUVTex : MonoBehaviour
{
    // Start is called before the first frame update

    Material myMat;
    float scrollSpeed = 8f;

    void Start()
    {
        myMat = gameObject.GetComponent<Renderer>().material;
        scrollSpeed = scrollSpeed * (-1);
    }

    // Update is called once per frame
    void Update()
    {
        float offset = Time.time * scrollSpeed;
        myMat.mainTextureOffset = new Vector2(0, offset);
    }
}
